package com.algomon.game.system

import com.algomon.game.component.Direction
import com.algomon.game.component.MoveComponent
import com.algomon.game.component.PhysicComponent
import com.github.quillraven.fleks.AllOf
import com.github.quillraven.fleks.ComponentMapper
import com.github.quillraven.fleks.Entity
import com.github.quillraven.fleks.IteratingSystem
import ktx.math.component1
import ktx.math.component2

@AllOf([MoveComponent::class, PhysicComponent::class])
class MoveSystem (
    private val moveCmps:ComponentMapper<MoveComponent>,
    private val physicCmps:ComponentMapper<PhysicComponent>
):IteratingSystem(){
    override fun onTickEntity(entity: Entity) {
        val moveCmp = moveCmps[entity]
        val physicCmp = physicCmps[entity]
        val mass = physicCmp.body.mass
        val (velX,velY) = physicCmp.body.linearVelocity
        val directionChecker = moveCmp.direction

        if(moveCmp.cos == 0f && moveCmp.sin == 0f){
            physicCmp.impulse.set(
                mass * (0f - velX),
                mass * (0f - velY)
            )
            return
        }


        if (moveCmp.cos > 0f) moveCmp.direction = Direction.RIGHT
        else if(moveCmp.cos < 0f) moveCmp.direction = Direction.LEFT
        if (moveCmp.sin > 0f) moveCmp.direction = Direction.BACK
        else if(moveCmp.sin < 0f) moveCmp.direction = Direction.FRONT
        if(directionChecker != moveCmp.direction) moveCmp.changeDirection = true

        physicCmp.impulse.set(
            mass * (moveCmp.speed * moveCmp.cos - velX),
            mass * (moveCmp.speed * moveCmp.sin - velY)
        )
    }
}
