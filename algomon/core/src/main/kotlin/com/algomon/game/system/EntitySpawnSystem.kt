package com.algomon.game.system

import com.algomon.game.Main.Companion.UNIT_SCALE
import com.algomon.game.component.AiComponent
import com.algomon.game.component.AnimationComponent
import com.algomon.game.component.AnimationModel
import com.algomon.game.component.AnimationType
import com.algomon.game.component.CollisionComponent
import com.algomon.game.component.DEFAULT_SPEED
import com.algomon.game.component.ImageComponent
import com.algomon.game.component.InteractComponent
import com.algomon.game.component.InteractableComponent
import com.algomon.game.component.MoveComponent
import com.algomon.game.component.PhysicComponent
import com.algomon.game.component.PhysicComponent.Companion.physicCmpFromImage
import com.algomon.game.component.PlayerComponent
import com.algomon.game.component.SpawnCfg
import com.algomon.game.component.SpawnComponent
import com.algomon.game.component.StateComponent
import com.algomon.game.event.MapChangeEvent
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType.StaticBody
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.scenes.scene2d.Event
import com.badlogic.gdx.scenes.scene2d.EventListener
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.utils.Scaling
import com.github.quillraven.fleks.AllOf
import com.github.quillraven.fleks.ComponentMapper
import com.github.quillraven.fleks.Entity
import com.github.quillraven.fleks.IteratingSystem
import ktx.app.gdxError
import ktx.box2d.box
import ktx.math.vec2
import ktx.tiled.layer
import ktx.tiled.type
import ktx.tiled.x
import ktx.tiled.y

@AllOf([SpawnComponent::class])
class EntitySpawnSystem(
    private val phWorld: World,
    private val atlas: TextureAtlas,
    private val spawnCmps: ComponentMapper<SpawnComponent>
) : EventListener, IteratingSystem(){
    private val cacheCfgs = mutableMapOf<String, SpawnCfg>()
    private val cacheSize = mutableMapOf<AnimationModel, Vector2>()

    override fun onTickEntity(entity: Entity) {
        with(spawnCmps[entity]){
            val cfg = spawnCfg(type)
            val relativeSize = size(cfg.model)

            world.entity{
                val imageCmp = add<ImageComponent>{
                    image = Image().apply {
                        setPosition(location.x, location.y)
                        setSize(relativeSize.x, relativeSize.y)
                        setScaling(Scaling.fill)
                    }
                }

                add<AnimationComponent> {
                    nextAnimation(cfg.model, AnimationType.idleFront)
                }

                physicCmpFromImage(phWorld, imageCmp.image, cfg.bodyType){ phCmp, width, height ->
                    val w = width * cfg.physicScaling.x
                    val h = height * cfg.physicScaling.y
                    phCmp.offset.set(cfg.physicOffset)
                    phCmp.size.set(w,h)

                    // Sensor Box
                    box(w, h, cfg.physicOffset){
                        isSensor = cfg.bodyType != StaticBody
                        userData = HIT_BOX_SENSOR
                    }

                    // Collision Box
                    if (cfg.bodyType != StaticBody){
                        val collH = h * 0.4f
                        val collOffset = vec2().apply { set(cfg.physicOffset) }
                        collOffset.y -= h * 0.5f - collH * 0.5f

                        box(w, collH, collOffset)
                    }
                }

                if (cfg.speedScaling > 0f) {
                    add<MoveComponent>{
                        speed = DEFAULT_SPEED * cfg.speedScaling
                    }
                }

                if (type == "Player"){
                    add<PlayerComponent>()
                    add<InteractComponent>{
                        maxDelay = cfg.interactDelay
                        extraRange = cfg.interactExtraRange
                    }
                    add<StateComponent>()
                }

                if (cfg.interactable){
                    add<InteractableComponent>()
                }

                if(cfg.bodyType != StaticBody){
                    add<CollisionComponent>()
                }

                if(cfg.aiTreePath.isNotBlank()){
                    add<AiComponent>{
                        treePath = cfg.aiTreePath
                    }
                }
            }
        }
        world.remove(entity)
    }

    private fun spawnCfg(type: String): SpawnCfg = cacheCfgs.getOrPut(type) {
        when(type){
            "Player" -> SpawnCfg(
                AnimationModel.player,
                physicScaling = vec2(0.3f, 0.55f),
                physicOffset = vec2(0f, 0f * UNIT_SCALE),
                interactDelay = 0.05f,
                interactExtraRange = 0.6f,
            )
            "Slime" -> SpawnCfg(
                AnimationModel.slime,
                physicScaling = vec2(0.3f, 0.3f),
                physicOffset = vec2(0f, -2f * UNIT_SCALE),
                aiTreePath = "ai/npc.tree"
            )
            "Door" -> SpawnCfg(
                AnimationModel.door,
                physicScaling = vec2(1f, 0.9f),
                physicOffset = vec2(0f, 0f * UNIT_SCALE),
                interactable = true,
                bodyType = StaticBody
            )
            "Door2" -> SpawnCfg(
                AnimationModel.door2,
                physicScaling = vec2(1f, 0.9f),
                physicOffset = vec2(0f, 0f * UNIT_SCALE),
                interactable = true,
                bodyType = StaticBody
            )
            "Shelf" -> SpawnCfg(
                AnimationModel.shelf,
                physicScaling = vec2(0.5f, 0.2f),
                physicOffset = vec2(0f, -6f * UNIT_SCALE),
                interactable = true,
                bodyType = StaticBody
            )
            "Exit" -> SpawnCfg(
                AnimationModel.exit,
                physicScaling = vec2(1f, 1f),
                physicOffset = vec2(0f, -1f * UNIT_SCALE),
                interactable = true,
                bodyType = StaticBody
            )
            "Computer" -> SpawnCfg(
                AnimationModel.computer,
                physicScaling = vec2(1f, 0.5f),
                physicOffset = vec2(0f, -8f * UNIT_SCALE),
                interactable = true,
                bodyType = StaticBody
            )
            "Computer2" -> SpawnCfg(
                AnimationModel.computer2,
                physicScaling = vec2(1f, 0.5f),
                physicOffset = vec2(0f, -8f * UNIT_SCALE),
                interactable = true,
                bodyType = StaticBody
            )
            else -> gdxError("Type $type has no SpawnCfg setup")
        }
    }

    private fun size(model: AnimationModel) = cacheSize.getOrPut(model){
        val regions = atlas.findRegions("${model.atlasKey}/${AnimationType.idleFront.atlasKey}")
        if(regions.isEmpty){
            gdxError("There are no regions for the idleFront animation of model $model")
        }
        val firstFrame = regions.first()
        vec2(firstFrame.originalWidth * UNIT_SCALE, firstFrame.originalHeight * UNIT_SCALE)
    }

    override fun handle(event: Event?): Boolean {
        when(event){
            is MapChangeEvent -> {
                world.removeAll()
                val entityLayer = event.map.layer("entities")
                entityLayer.objects.forEach { mapObj ->
                    val type = mapObj.type ?: gdxError("MapObject $mapObj does not have a type")
                    world.entity {
                        add<SpawnComponent> {
                            this.type = type
                            this.location.set(mapObj.x * UNIT_SCALE, mapObj.y * UNIT_SCALE)
                        }
                    }
                }
                return true
            }
        }
        return false
    }

    companion object {
        const val HIT_BOX_SENSOR = "Hitbox"
    }
}
