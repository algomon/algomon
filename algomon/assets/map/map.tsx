<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="map" tilewidth="16" tileheight="16" tilecount="1424" columns="16">
 <image source="img/Interiors_free_16x16.png" width="256" height="1424"/>
 <tile id="83">
  <objectgroup draworder="index" id="2">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="112">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3" y="0" width="13" height="16"/>
  </objectgroup>
 </tile>
 <tile id="114">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="13" height="16"/>
  </objectgroup>
 </tile>
 <tile id="128">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3" y="0" width="13" height="16"/>
  </objectgroup>
 </tile>
 <tile id="129">
  <objectgroup draworder="index" id="2">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="130">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="13" height="16"/>
  </objectgroup>
 </tile>
 <tile id="147">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="310">
  <objectgroup draworder="index" id="2">
   <object id="2" x="7" y="0" width="9" height="16"/>
  </objectgroup>
 </tile>
 <tile id="311">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="10" height="16"/>
  </objectgroup>
 </tile>
 <tile id="326">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6" y="0" width="10" height="8"/>
  </objectgroup>
 </tile>
 <tile id="327">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="11" height="8"/>
  </objectgroup>
 </tile>
 <tile id="609">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1" y="14.0909" width="14" height="1.90909"/>
  </objectgroup>
 </tile>
 <tile id="625">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1" y="0" width="14" height="9"/>
  </objectgroup>
 </tile>
</tileset>
